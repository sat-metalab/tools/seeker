(
// ServerOptions.inDevices;
// ServerOptions.outDevices;
Server.default.options.inDevice_("BlackHole 16ch");
Server.default.options.outDevice_("Built-in Output");
Server.default.recHeaderFormat = "wav";
Server.default.recSampleFormat = "int24";
~config = SatieConfiguration(s,
	listeningFormat: [\monoSpat, \monoSpat, \octoVBAP, \monoSpat],
	// listeningFormat: [\stereoListener],
	outBusIndex: [0, 1, 2, 10],   // zero based offset
	// outBusIndex: [20] // theatre
	userPluginsDir: thisProcess.nowExecutingPath.dirname+/+"plugins"
);
~config.server.options.numInputBusChannels = 16;
~satie = Satie(~config);
// ~satie.boot;
~satie.waitForBoot({
	~satie.makeSynthDef(\SeekerIn, \MonoIn, [], [], [], ~satie.config.listeningFormat, ~satie.config.outBusIndex, paramsMapper: \seeker4layers);
	s.meter;
	s.plotTree;
})
)

s.record(path: "~/Desktop/Seeker-%.wav".format(Date.getDate.format("%d-%m-%y_%H:%M:%S")).standardizePath, bus: 0, numChannels: 8);

s.stopRecording;
